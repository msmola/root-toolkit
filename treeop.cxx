#include <stdexcept>
#include <string>
#include <sstream>
#include <memory>
#include <boost/algorithm/string.hpp>
#include "TTree.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TROOT.h"
#include "TChain.h"
#include "Filelist.h"
#include "common.h"
#include "Path.h"
#include "RTKException.h"
#include "treeop.h"

namespace rtk {


void fill(const std::map<std::string, docopt::value> args, std::ostream& os) {
    try {
        auto prefix = getStringArg("--prefix", args);
        auto suffix = getStringArg("--suffix", args);
        Path path(pipeArg(getArg("<path>", args)), prefix, suffix);
        Filelist files = Filelist(path.getFile());

        std::string arghash = hash(args, 8);
        std::ostringstream filepath;
        filepath << "/tmp/rtk_" << arghash << ".root";
        if (!getArg("--no-cache", args).asBool() && file_exists(filepath.str().c_str())) {
            if (getArg("--debug", args).asBool())
                std::cerr << "load from cache: " << filepath.str() << "/htemp" << std::endl;
            os << filepath.str() << "/htemp" << std::endl;
            return;
        }

        std::unique_ptr<TChain> chain = files.getChain(path.getObject());

        for (auto &more : args.at("<more>").asStringList()) {
            Path morepath(more, prefix, suffix);
            Filelist morefiles(morepath.getFile());
            std::unique_ptr<TChain> morechain = morefiles.getChain(morepath.getObject());
            // TODO Potential memory leak
            chain->Add(morechain.release());
        }

        gROOT->SetBatch(true);

        std::ostringstream varexp;
        varexp << args.at("<varexp>").asString();

        std::vector<std::string> binning;
        if (args.at("--nbins"))
            binning.emplace_back(args.at("--nbins").asString());
        if (args.at("--nbinsx"))
            binning.emplace_back(args.at("--nbinsx").asString());
        if (args.at("--min"))
            binning.emplace_back(args.at("--min").asString());
        if (args.at("--xmin"))
            binning.emplace_back(args.at("--xmin").asString());
        if (args.at("--max"))
            binning.emplace_back(args.at("--max").asString());
        if (args.at("--xmax"))
            binning.emplace_back(args.at("--xmax").asString());
        if (args.at("--nbinsy"))
            binning.emplace_back(args.at("--nbinsx").asString());
        if (args.at("--ymin"))
            binning.emplace_back(args.at("--ymin").asString());
        if (args.at("--ymax"))
            binning.emplace_back(args.at("--ymax").asString());

        if (!binning.empty())
            varexp << ">>histo(" << boost::algorithm::join(binning, ",") << ")";

        std::ostringstream selection;

        if (args.at("--scale")) {
            std::vector<std::string> scaleexp = args.at("--scale").asStringList();
            for (auto e : scaleexp)
                selection << e << "*";
        }

        if (args.at("--selection"))
            selection << "(" << args.at("--selection").asString() << ")";
        else
            selection << "1";

        if (args.at("--debug").asBool())
            std::cerr << varexp.str() << "\n" << selection.str() << std::endl;


        chain->Draw(
                varexp.str().c_str(),
                selection.str().c_str(),
                "e",
                args.at("--nentries")
                    ? args.at("--nentries").asLong()
                    : TTree::kMaxEntries
                );

        std::vector<std::string> vars;
        boost::algorithm::split(vars, args.at("<varexp>").asString(), boost::is_any_of(";"));

        TH1* histo = chain->GetHistogram();
        std::ostringstream title;
        if (args.at("--title"))
            title << args.at("--title").asString();
        else
            title << path.getFile() << " " << path.getObject() << " " << args.at("<varexp>").asString();
        title << ";" << vars.at(0);
        if (histo->InheritsFrom("TH2"))
            title << ";" << vars.at(1);
        title << ";" << "Entries";
        histo->SetTitle(title.str().c_str());

        if (args.at("--color")) {
            Color_t color = getColor(args.at("--color").asString());
            histo->SetLineColor(color);
            histo->SetFillColor(color);
            histo->SetMarkerColor(color);
        }

        histo->SetName("htemp");

        for (auto& outpath : getArg("--save", args).asStringList())
            os << write(histo, outpath, getArg("--recreate", args).asBool()) << std::endl;
        if (args.at("--save").asStringList().empty())
            os << cache(arghash.c_str(), histo) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Fill failed: ") << e.what();
    }
}

void sum(const std::map<std::string, docopt::value> args, std::ostream& os) {
    try {
        Path path(pipeArg(args.at("<path>")));
        Filelist files(path.getFile());

        std::unique_ptr<TChain> chain = files.getChain(path.getObject());

        for (auto &more : args.at("<more>").asStringList()) {
            Path morepath(more);
            Filelist morefiles(morepath.getFile());
            std::unique_ptr<TChain> morechain = morefiles.getChain(morepath.getObject());
            // TODO potential memory leak
            chain->Add(morechain.release());
        }

        TTreeReader reader(chain.get());
        TLeaf* leaf = chain->GetLeaf(getArg("<branch>", args).asString().c_str());
        std::string vtype(leaf->GetTypeName());
        if (vtype == "Double_t" || vtype == "double") {
            TTreeReaderValue<double> value(reader, args.at("<branch>").asString().c_str());
            if (value.GetSetupStatus() < 0)
                throw RTKException("Failed to read value of type \"double\" from branch ")
                    << args.at("<branch>").asString() << ".";
            double out = 0;
            while (reader.Next())
                out += *value;

            os << out << std::endl;
        } else if (vtype == "Float_t" || vtype == "float") {
            TTreeReaderValue<float> value(reader, args.at("<branch>").asString().c_str());
            //if (value.GetSetupStatus() < 0)
            //    throw RTKException("Failed to read value of type \"float\" from branch ")
            //        << args.at("<branch>").asString() << ".";
            double out = 0;
            while (reader.Next())
                out += *value;

            os << out << std::endl;
        } else {
            throw RTKException("Sum over type ") << vtype << " is currently not supported.";
        }
        
    } catch (std::exception &e) {
        throw RTKException("RTK Sum failed: ") << e.what();
    }
}


void entry(std::map<std::string, docopt::value> args, std::ostream& os) {
    try {
        Path path(pipeArg(args.at("<path>")));
        Filelist files(path.getFile());

        std::unique_ptr<TChain> chain = files.getChain(path.getObject());

        TLeaf* leaf = chain->GetLeaf(getArg("<branch>", args).asString().c_str());
        std::string vtype(leaf->GetTypeName());
        if (args.at("--debug").asBool())
            std::cerr << "Get entry of type " << vtype << std::endl;

        long thisEntry = getArg("<entry>", args).asLong();

        if (vtype == "Int_t") {
            os << leaf->GetTypedValue<Int_t>(thisEntry) << std::endl;
        } else {
            throw RTKException("Type ") << vtype << " is currently not supported.";
        }
    } catch (std::exception &e) {
        throw RTKException("RTK Entry failed: ") << e.what();
    }
}

}
