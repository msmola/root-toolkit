
using namespace rtk;

static const char usage[] =
R"(Root Toolkit

    Usage:
        rtk [--debug] ls [-dl] <path>
        rtk [--debug] draw [-s <outfile>... --palette <palette> --style <style> --legend --noyields --text-font <textfont> --text-size <textsize> --batch --title <title> --logx --logy --logz --ratio --xmin <xmin> --xmax <xmax> --ymin <ymin> --ymax <ymax> --zmin <zmin> --zmax <zmax> --text <text>... (--atlas [--atlas-label <label>]) --title-offset-x <offset> --title-offset-y <offset> --title-offset-z <offset> --margin-top <margin> --margin-right <margin> --margin-bottom <margin> --margin-left <margin> --padding-top <padding> --padding-right <padding> --padding-left <padding> (--cme <cme> [--lumi <lumi>]) --xtitle <xtitle> --ytitle <ytitle> --ztitle <ztitle> --width <width> --height <height> --paint-text-format <format> --text-offset <offset>] [<histo>...|-] [stack <histo>...]
        rtk [--debug] print [<path>|-]
        rtk [--debug] fill [-n --selection <selection> --scale <scaleexp>... --title <title> --color <color> --nentries <nentries> -s <outfile>... --recreate --prefix <prefix> --suffix <suffix>] [--nbins <nbins> --min <min> --max <max>] <varexp> [<path>|-] [<more>...]
        rtk [--debug] fill [-n --selection <selection> --scale <scaleexp>... --title <title> --color <color> --nentries <nentries> -s <outfile>... --recreate --prefix <prefix> --suffix <suffix>] [--nbinsx <nbinsx> --xmin <xmin> --xmax <xmax> --nbinsy <nbinsy> --ymin <ymin> --ymax <ymax>] <varexp> [<path>|-] [<more>...]
        rtk [--debug] add [--title <title> --color <color> -s <outfile>... --recreate --prefix <prefix> --suffix <suffix>] [<path>|-] <other>...
        rtk [--debug] sub [--title <title> --color <color> -s <outfile>... --recreate] [<path>|-] <other>...
        rtk [--debug] mul [--title <title> --color <color> -s <outfile>... --recreate] [<path>|-] <other>...
        rtk [--debug] div [--title <title> --color <color> -s <outfile>... --recreate] [<path>|-] <other>...
        rtk [--debug] save [--recreate] [<source>|-] <target>
        rtk [--debug] scale [-s <outfile>... --recreate --multiply-by <mul>... --divide-by <div>... --prefix <prefix> --suffix <suffix>] <factor> [<path>|-] [<more>...]
        rtk [--debug] sum [--type <type>] <branch> [<path>|-] [<more>...]
        rtk [--debug] entry [--type <type>] <entry> <branch> [<path>|-]
        rtk [--debug] entries [--effective|--fast|--selection <selection> --prefix <prefix> --suffix <suffix>] [<path>|-] [<more>...]
        rtk [--debug] integral [--inclusive (--pmerror|--nlerror) --prefix <prefix> --suffix <suffix>] [<path>|-] [<more>...]
        rtk [--debug] sumweights [--inclusive] [<path>|-] [<more>...]
        rtk [--debug] projection --axis <axis> [<path>|-]
        rtk [--debug] set [-i --color <color> --title <title>] [<path>|-]
        rtk [--debug] binning [--axis <axis>] [<path>|-]
        rtk [--debug] rebin [--title <title> --color <color> --prefix <prefix> --suffix <suffix>] <edges> [<path>|-] [<more>...]
        rtk [--debug] rebin --merge <nbins> [--title <title> --color <color> --prefix <prefix> --suffix <suffix>] [<path>|-] [<more>...]
        rtk [--debug] min [--prefix <prefix> --suffix <suffix>] [<path>|-] [<more>...]
        rtk [--debug] max [--prefix <prefix> --suffix <suffix>] [<path>|-] [<more>...]
        rtk [--debug] avg [--prefix <prefix> --suffix <suffix>] [<path>|-] [<more>...]
        rtk -h|--help
        rtk --version

    Options:
        -d --directory                      List only directories.
        -l --long                           Show more info.
        -b --batch                          Run in batch mode.
        -s --save <outfile>...              Save Drawn histogram to outfile
           --width <width>                  Set canvas width [default: 800]
           --height <height>                Set canvas height [default: 925]
           --prefix <prefix>                Add a prefix to all paths [default: ]
           --suffix <suffix>                Add a suffix to all paths [default: ]
           --selection <selection>          Event Selection
           --scale <scaleexp>               Scale factor
           --multiply-by <mul>...           Multiply by a factor
           --divide-by <div>...             Divide by a factor
           --title <title>                  Histogram title
           --nbins <nbins>                  Number of bins for 1D histogram
           --min <min>                      Minimum for 1D histogram
           --max <max>                      Maximum for 1D histogram
           --nbinsx <nbinsx>                Number of bins on x-axis for 2D histogram
           --xmin <xmin>                    Minimum on x-axis
           --xmax <xmax>                    Maximum on x-axis
           --nbinsy <nbinsy>                Number of bins on y-axis for 2D histogram
           --ymin <ymin>                    Minimum on y-axis
           --ymax <ymax>                    Maximum on y-axis
           --zmin <zmin>                    Minimum on z-axis
           --zmax <zmax>                    Maximum on z-axis
           --color <color>                  Set Line, Fill and marker color
           --palette <palette>              Palette to draw histogram colors from
           --style <style>                  Drawing Style for histograms
           --legend                         Add Legend to histogram
           --noyields                       Don't show yields in legend
           --text-font <textfont>           Set default text font [default: 43]
           --text-size <textsize>           Set default text size [default: 22]
           --type <type>                    Branch Type [default: double]
           --nentries <nentries>            Set Maximum Entries to run over
        -n --no-cache                       Ignore Cache
           --effective                      Return effective entries for histograms
           --fast                           Use GetEntriesFast on TTrees
           --inclusive                      Include overflow bins
           --logx                           Log scale for x-axis
           --logy                           Log scale for y-axis
           --logz                           Log scale for z-axis
           --axis <axis>                    Which axis to project to
           --pmerror                        Output error as 'value+-error'
           --nlerror                        Output error on new line ('value\nerror')
           --ratio                          Add ratio plot
           --text <text>                    Add test to plots
           --atlas                          Add ATLAS-Logo
           --atlas-label <label>            Add ATLAS-label
           --lumi <lumi>                    Set Luminosity
           --cme <cme>                      Set centre-of-mass energy
           --recreate                       Recreate file when saving
           --xtitle <xtitle>                X-axis title
           --ytitle <ytitle>                Y-axis title
           --ztitle <ztitle>                Z-axis title
           --title-offset-x <offset>        X-axis title offset [default: 1.3]
           --title-offset-y <offset>        Y-axis title offset [default: 2.5]
           --title-offset-z <offset>        Z-axis title offset [default: 2.5]
           --text-offset <offset>           Add an offset for text [default: 0]
           --margin-top <margin>            Top Margin
           --margin-right <margin>          Right Margin
           --margin-bottom <margin>         Bottom Margin
           --margin-left <margin>           Left Margin
           --padding-top <padding>          Top Padding
           --padding-right <padding>        Right Padding
           --padding-left <padding>         Left Padding
           --paint-text-format <format>     Format text if histogram option TEXT is used
           --merge <nbins>                  Rebin by merging <nbins> at a time
           --debug                          Print debug output
        -h --help                           Show this screen.
           --version                        Show rtk version and exit.
)";

