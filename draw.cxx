#include <memory>
#include <limits>
#include <boost/algorithm/string.hpp>
#include "TH1.h"
#include "THStack.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TRatioPlot.h"
#include "TPaveText.h"
#include "TLatex.h"
#include "docopt.h"
#include "Filelist.h"
#include "Path.h"
#include "common.h"
#include "draw.h"
#include "RTKException.h"

namespace rtk {

const std::string makeLabel(const TH1* histo, bool yields) {
    std::ostringstream label;
    label << histo->GetTitle();
    if (yields)
        label << "  " << histo->Integral();
    return label.str();
}


void draw(const std::map<std::string, docopt::value> args, std::ostream& os) {
    try {
        std::vector<std::string> paths;
        std::vector<std::string> stackpaths;

        {
            auto current = &paths;
            for (const std::string& path : getArg("<histo>", args).asStringList()) {
                if (path == "stack") {
                    current = &stackpaths;
                    continue;
                }
                current->emplace_back(path);
            }
        }

        if (paths.empty() && stackpaths.empty()) {
            if (getArg("--debug", args).asBool())
                std::cerr << "waiting for input..." << std::endl;
            paths.emplace_back();
            std::cin >> paths.back();
        }

        if (getArg("--debug", args).asBool()) {
            std::cerr << "Draw ";
            for (auto path : paths)
                std::cerr << path << " ";
            std::cerr << "stack ";
            for (auto path : stackpaths)
                std::cerr << path << " ";
            std::cerr << std::endl;
        }


        TApplication* app = nullptr;
        if (!getArg("--batch", args).asBool())
            app = new TApplication("RTK Draw", 0, nullptr);

        Style_t textFont = getArg("--text-font", args).asLong();
        Float_t textSize = getArg("--text-size", args).asLong();

        gStyle->SetOptStat(0);
        gStyle->SetOptTitle(0);
        gStyle->SetTextFont(textFont);
        gStyle->SetTextSize(textSize);
        gStyle->SetMarkerSize(1.2);
        gStyle->SetMarkerStyle(20);
        gStyle->SetLineWidth(2.);
        //gStyle->SetErrorX(0.0001);
        gStyle->SetEndErrorSize(0.);
        if (getArg("--paint-text-format", args))
            gStyle->SetPaintTextFormat(getStringArg("--paint-text-format", args).c_str());

        TCanvas canvas(
                "RTK Draw",
                "RTK Draw",
                getDoubleArg("--width", args),
                getDoubleArg("--height", args)
                );
        TVirtualPad* pad = canvas.cd();


        if (getArg("--logx", args).asBool())
            canvas.SetLogx();
        if (getArg("--logy", args).asBool())
            canvas.SetLogy();
        if (getArg("--logz", args).asBool())
            canvas.SetLogz();

        if (getArg("--palette", args))
            gStyle->SetPalette(getPalette(getArg("--palette", args).asString()));

        const std::string stackopt = "HIST";
        const std::string samestackopt = "HIST SAME";
        const std::string histopt = getArg("--style", args)
            ? args.at("--style").asString()
            : "E1";
        const std::string sameopt(histopt + " SAME");
        double xmin = std::numeric_limits<double>::max();
        double xmax = std::numeric_limits<double>::min();
        double ymax = std::numeric_limits<double>::min();
        double ymin = std::numeric_limits<double>::max();
        double zmax = std::numeric_limits<double>::min();
        double zmin = std::numeric_limits<double>::max();
        bool is2D = false;
        const bool isColz = histopt.find("COLZ") != std::string::npos
            || histopt.find("colz") != std::string::npos;
        if (DEBUG_ENABLED() && isColz)
            std::cerr << "Style is COLZ" << std::endl;
        std::shared_ptr<TH1> main;
        std::unique_ptr<THStack> stack;
        std::vector<std::shared_ptr<TH1> > other;
        std::string xlabel, ylabel, zlabel;
        std::vector<std::unique_ptr<TH1> > hstack;
        std::unique_ptr<TRatioPlot> ratio;

        if (getArg("--xtitle", args)) xlabel = args.at("--xtitle").asString();
        if (getArg("--ytitle", args)) ylabel = args.at("--ytitle").asString();
        if (getArg("--ztitle", args)) zlabel = args.at("--ztitle").asString();

        // ======================================================== SET UP STACK
        if (!stackpaths.empty()) {
            stack = std::make_unique<THStack>();

            for (auto &path : stackpaths) {
                hstack.emplace_back();
                hstack.back() = Path(path).getHistogram();
                TH1* histo = hstack.back().get();
                histo->SetLineColor(1);
                stack->Add(histo);
                xmin = std::min(xmin, histo->GetXaxis()->GetXmin());
                xmax = std::max(xmax, histo->GetXaxis()->GetXmax());
                ymin = std::min(ymin, stack->GetMinimum());
                ymax = std::max(ymax, stack->GetMaximum());
                if (xlabel.empty())
                    xlabel = histo->GetXaxis()->GetTitle();
                else
                    histo->GetXaxis()->SetTitle(xlabel.c_str());
                if (ylabel.empty())
                    ylabel = histo->GetYaxis()->GetTitle();
                if (ylabel.empty())
                    ylabel = "Events";
                histo->GetYaxis()->SetTitle(ylabel.c_str());
            }
        }

        for (auto &path : paths) {
            std::shared_ptr<TH1> histo(Path(path).getHistogram().release());
            histo->SetMarkerStyle(20);
            histo->SetMarkerSize(1.2);
            if (!main) {
                // ======================================= SET UP MAIN HISTOGRAM
                main = histo;
                if (xlabel.empty())
                    xlabel = histo->GetXaxis()->GetTitle();
                else
                    histo->GetXaxis()->SetTitle(xlabel.c_str());
                if (histo->InheritsFrom("TH2")) {
                    is2D = true;
                    if (DEBUG_ENABLED()) std::cerr << "Read 2D histogram" << std::endl;
                    if (ylabel.empty())
                        ylabel = histo->GetYaxis()->GetTitle();
                    if (zlabel.empty())
                        zlabel = histo->GetZaxis()->GetTitle();
                    if (zlabel.empty())
                        zlabel = "Events";
                    histo->GetZaxis()->SetTitle(zlabel.c_str());
                    ymin = std::min(ymin, histo->GetYaxis()->GetXmin());
                    ymax = std::max(ymax, histo->GetYaxis()->GetXmax());
                    zmin = std::min(zmin, histo->GetMinimum());
                    zmax = std::max(zmax, histo->GetMaximum());

                    if (isColz)
                        histo->SetMarkerSize(1000);
                } else {
                    if (ylabel.empty())
                        ylabel = histo->GetYaxis()->GetTitle();
                    if (ylabel.empty())
                        ylabel = "Events";
                    histo->GetYaxis()->SetTitle(ylabel.c_str());
                    ymax = std::max(ymax, histo->GetMaximum());
                    ymin = std::min(ymin, histo->GetMinimum());
                }
            } else if (is2D) {
                throw RTKException("Cannot draw multiple 2D histograms");
            } else {
                // ===================================== SET UP OTHER HISTOGRAMS
                histo->GetXaxis()->SetTitle(xlabel.c_str());
                histo->GetYaxis()->SetTitle(ylabel.c_str());
                if (!zlabel.empty())
                    histo->GetZaxis()->SetTitle(zlabel.c_str());
                histo->SetStats(0);
                ymax = std::max(ymax, histo->GetMaximum());
                ymin = std::min(ymin, histo->GetMinimum());
                other.push_back(histo);
            }
            xmin = std::min(xmin, histo->GetXaxis()->GetXmin());
            xmax = std::max(xmax, histo->GetXaxis()->GetXmax());
        }

        if (getArg("--ymax", args)) ymax = std::stod(args.at("--ymax").asString());
        if (getArg("--ymin", args))
            ymin = std::stod(args.at("--ymin").asString());
        else if (!is2D) {
            if (ymin == 0.)
                ymin = std::max(1., ymax * 0.0001);
            else 
                ymin = std::max(ymin, ymax * 0.0001);
        }

        // ===================================================== DRAW RATIO PLOT
        if (getArg("--ratio", args).asBool()) {
            if (!stack)
                throw RTKException("Cannot draw ratio plot without a stack as reference");
            if (!other.empty())
                throw RTKException("Ratio may be drawn only for exactly one ")
                    << "main histogram + stack";

            std::unique_ptr<TH1> stackSum((TH1*)hstack.at(0)->Clone());
            stackSum->Reset();
            for (auto& histo : hstack)
                stackSum->Add(histo.get());

            ratio = std::make_unique<TRatioPlot>(main.get(), stackSum.get(), "divsymm");
            ratio->Draw();
            ratio->SetUpBottomMargin(0.);
            TPad* upper = ratio->GetUpperPad();
            pad = upper->cd();
            stackSum->Delete();
            main->SetDrawOption(histopt.c_str());


            ratio->GetLowerRefGraph()->SetMinimum(0.5);
            ratio->GetLowerRefGraph()->SetMaximum(1.5);
            ratio->GetLowYaxis()->SetNdivisions(8);

            stack->Draw(samestackopt.c_str());

        } else {
            // ====================================================== DRAW BASIC
            if (stack) {
                stack->Draw(stackopt.c_str());
                if (main) main->Draw(sameopt.c_str());
            } else {
                main->SetOption(histopt.c_str());
                main->Draw();
            }
            for (auto& o : other)
                o->Draw(sameopt.c_str());
        }

        Float_t topMargin;
        Float_t leftMargin;
        Float_t rightMargin;
        Float_t bottomMargin;
        Float_t leftPadding;
        Float_t topPadding;
        Float_t rightPadding;
        Float_t textOffset = getDoubleArg("--text-offset", args)/pad->GetWh();

        if (getArg("--margin-top", args)) topMargin = getDoubleArg("--margin-top", args)/pad->GetWh();
        else topMargin = 40./pad->GetWh();

        if (getArg("--margin-right", args)) rightMargin = getDoubleArg("--margin-right", args)/pad->GetWw();
        else if (isColz) rightMargin = 155./pad->GetWw();
        else rightMargin = 40./pad->GetWw();

        if (getArg("--margin-bottom", args)) bottomMargin = getDoubleArg("--margin-bottom", args)/pad->GetWh();
        else bottomMargin = 85./pad->GetWh();

        if (getArg("--margin-left", args)) leftMargin = getDoubleArg("--margin-left", args)/pad->GetWw();
        else leftMargin = 115./pad->GetWw();

        if (getArg("--padding-top", args)) topPadding = getDoubleArg("--padding-top", args)/pad->GetWh();
        else topPadding = 35./pad->GetWh();

        if (getArg("--padding-right", args)) rightPadding = getDoubleArg("--padding-right", args)/pad->GetWw();
        else rightPadding = 20./pad->GetWw();

        if (getArg("--padding-left", args)) leftPadding = getDoubleArg("--padding-left", args)/pad->GetWw();
        else leftPadding = 20./pad->GetWw();

        pad->SetTopMargin(topMargin);
        pad->SetLeftMargin(leftMargin);
        pad->SetRightMargin(rightMargin);
        pad->SetBottomMargin(bottomMargin);
        pad->SetTicks(1,1);
        pad->SetLineWidth(1);

        std::pair<Int_t, Int_t> textRows{0,0};

        canvas.cd();

        // ============================================================== LEGEND
        if (getArg("--legend", args).asBool()) {
            if (is2D) throw RTKException("Option --legend is not supported with 2D histograms.");

            auto* leg = new TLegend(
                    0.51,
                    0.8,
                    1 - rightMargin - rightPadding,
                    1 - topMargin - topPadding,
                    "",
                    "nbNDC"
                    );
            leg->SetBorderSize(0);
            leg->SetFillStyle(0);
            leg->SetTextFont(textFont);
            leg->SetTextSize(textSize);
            const char* lstyle = args.at("--style") ? "lpf" : "lp";
            bool yields = !getArg("--noyields", args).asBool();
            if (main) leg->AddEntry(main.get(), makeLabel(main.get(), yields).c_str(), lstyle);
            for (auto& histo : other)
                leg->AddEntry(histo.get(), makeLabel(histo.get(), yields).c_str(), lstyle);
            
            if (stack) {
                for (auto iter = hstack.end() - 1; iter >= hstack.begin(); --iter)
                    leg->AddEntry((*iter).get(), makeLabel((*iter).get(), yields).c_str(), "f");
            }
            if (DEBUG_ENABLED())
                std::cerr << "Legend at 0.51 "
                    << 1 - topMargin - topPadding - (leg->GetNRows() * (textSize + 4) / pad->GetWh())
                    << " " << 1 - rightMargin - rightPadding
                    << " " << 1 - topMargin - topPadding
                    << std::endl;
            leg->SetY1(1 - topMargin - topPadding - (leg->GetNRows() * (textSize + 4) / pad->GetWh()));
            leg->Draw();

            textRows.second = leg->GetNRows();
        }

        if (args.at("--ymin") || (!is2D && args.at("--logy").asBool()) || ymin < 0) {
            if (stack) stack->SetMinimum(ymin);
            if (main) main->SetMinimum(ymin);
        }
        if (getArg("--xmin", args)) xmin = std::stod(args.at("--xmin").asString());
        if (getArg("--xmax", args)) xmax = std::stod(args.at("--xmax").asString());

        if (getArg("--zmin", args) || args.at("--logz").asBool() || zmin < 0) {
            zmin = std::stod(args.at("--zmin").asString());
            main->SetMinimum(zmin);
        }
        if (getArg("--zmax", args)) {
            zmax = std::stod(args.at("--zmax").asString());
            main->SetMaximum(zmax);
        }



        if (stack) {
            TAxis* xaxis = stack->GetXaxis();
            TAxis* yaxis = stack->GetYaxis();
            xaxis->SetRangeUser(xmin, xmax);
            xaxis->SetTitleFont(textFont);
            xaxis->SetTitleSize(textSize * 1.2);
            yaxis->SetTitleFont(textFont);
            yaxis->SetTitleSize(textSize * 1.2);
            xaxis->SetLabelFont(textFont);
            xaxis->SetLabelSize(textSize);
            yaxis->SetLabelFont(textFont);
            yaxis->SetLabelSize(textSize);
            xaxis->SetTickSize(0.02);
            yaxis->SetTickSize(0.02);
            // set title offset only if default textSize
            if (getArg("--title-offset-x", args))
                xaxis->SetTitleOffset(getDoubleArg("--title-offset-x", args));
            if (getArg("--title-offset-y", args))
                yaxis->SetTitleOffset(getDoubleArg("--title-offset-y", args));
        }
        if (main) {
            TAxis* xaxis = main->GetXaxis();
            TAxis* yaxis = main->GetYaxis();
            main->SetYTitle(ylabel.c_str());
            xaxis->SetRangeUser(xmin, xmax);
            xaxis->SetTitleFont(textFont);
            xaxis->SetTitleSize(textSize * 1.2);
            yaxis->SetTitleFont(textFont);
            yaxis->SetTitleSize(textSize * 1.2);
            xaxis->SetLabelFont(textFont);
            xaxis->SetLabelSize(textSize);
            yaxis->SetLabelFont(textFont);
            yaxis->SetLabelSize(textSize);
            xaxis->SetTickSize(0.02);
            yaxis->SetTickSize(0.02);
            if (getArg("--title-offset-x", args))
                xaxis->SetTitleOffset(getDoubleArg("--title-offset-x", args));
            if (getArg("--title-offset-y", args))
                yaxis->SetTitleOffset(getDoubleArg("--title-offset-y", args));

            if (is2D) {
                yaxis->SetRangeUser(ymin, ymax);
                main->SetZTitle(zlabel.c_str());
                TAxis* zaxis = main->GetZaxis();
                zaxis->SetTitleFont(textFont);
                zaxis->SetTitleSize(textSize * 1.2);
                zaxis->SetLabelFont(textFont);
                zaxis->SetLabelSize(textSize);
                zaxis->SetTickSize(0.02);
                if (getArg("--title-offset-z", args))
                    zaxis->SetTitleOffset(getDoubleArg("--title-offset-z", args));
            }
        }
        for (auto& h : other)
            h->GetXaxis()->SetRangeUser(xmin, xmax);

        // ======================================================== ATLAS LABELS
        if (getArg("--atlas", args).asBool()) {
            Double_t atlasLeft = leftMargin + leftPadding + 0.02;
            Double_t atlasTop = 1 - topMargin - topPadding - 0.02;

            TLatex atlas;
            atlas.SetNDC();
            atlas.SetTextFont(72);
            atlas.SetTextSize(0.04);
            atlas.DrawLatex(atlasLeft, atlasTop, "ATLAS");

            textRows.first += 2;

            if (getArg("--atlas-label", args)) {
                double delx = 120. / pad->GetWw();
                TLatex atlasLabel;
                atlasLabel.SetTextFont(42);
                atlasLabel.SetTextSize(0.04);
                atlasLabel.DrawLatexNDC(
                        atlasLeft + delx,
                        atlasTop,
                        args.at("--atlas-label").asString().c_str()
                        );
            }

           textOffset += 0.04; 
        }

        // ====================================================== TITLE AND TEXT
        if (getArg("--title", args)
                || !getArg("--text", args).asStringList().empty()
                || getArg("--cme", args)) {

            auto* label = new TPaveText(
                    leftMargin + leftPadding,
                    0.8,
                    0.5,
                    1 - topMargin - topPadding - textOffset,
                    "nbNDC"
                    );

            label->SetTextAlign(11);
            label->SetTextFont(textFont);
            label->SetTextSize(textSize * 1.2);

            if (args.at("--cme")) {
                std::string cme("#sqrt{s} = ");
                cme += args.at("--cme").asString();
                if (getArg("--lumi", args)) {
                    cme += ", " + args.at("--lumi").asString();
                }
                label->AddText(cme.c_str());
            }

            if (args.at("--title")) 
                label->AddText(args.at("--title").asString().c_str());

            for (auto& text : getArg("--text", args).asStringList())
                label->AddText(text.c_str());

            double y1 = 1 - topMargin - topPadding - textOffset - ((textSize * 1.5) * label->GetSize() / pad->GetWh());
            if (DEBUG_ENABLED())
                std::cerr << "Add Label at " 
                    << leftMargin + leftPadding
                    << " " << y1
                    << " 0.5 "
                    << 1 - topMargin - topPadding - textOffset
                    << std::endl;

            label->SetY1(y1);
            label->SetFillStyle(0);
            label->SetBorderSize(0);
            label->Draw();

            textRows.first += label->GetSize();
        }

        // ============================================================= SCALING
        Int_t targetRows = std::max(textRows.first, textRows.second);

        if (is2D) {
            main->SetMaximum(zmax);
        } else {
            if (getArg("--logy", args).asBool()) {
                double scaleFactor = std::pow(ymax / ymin, ((topPadding * 0.8) + (textSize * 1.5 * targetRows / pad->GetWh() / (1 - topMargin - bottomMargin))));
                if (DEBUG_ENABLED())
                    std::cerr << "Scale ymax log times " << scaleFactor
                        << " for " << targetRows << " rows" << std::endl;
                ymax *= scaleFactor;
            } else {
                double spacing = (ymax - ymin) * ((topPadding * 0.8) + (textSize * 1.5 * targetRows / pad->GetWh() / (1 - topMargin - bottomMargin)));
                if (DEBUG_ENABLED())
                    std::cerr << "Scale ymax lin add " << spacing
                        << " for " << targetRows << " rows" << std::endl;
                ymax += spacing;
            }
            if (stack) stack->SetMaximum(ymax);
            if (main) main->SetMaximum(ymax);
            for (auto& h : other) h->SetMaximum(ymax);
        }

        // ================================================================ SAVE
        canvas.Update();

        // save or run
        for (auto& filepath : getArg("--save", args).asStringList())
            canvas.SaveAs(filepath.c_str());

        if (!getArg("--batch", args).asBool()) {
            canvas.Draw();
            app->Run();
            delete app;
        }

    } catch (std::exception &e) {
        throw RTKException("RTK Draw failed:\n") << e.what();
    }
}

}
