#ifndef RTK_PATH_H
#define RTK_PATH_H

#include <string>
#include <memory>
#include "TH1.h"
#include "Filelist.h"

namespace rtk {

class Path {
public:
    Path(std::string& input);
    Path(const char *input);
    Path(const std::string& input, const std::string& prefix = "", const std::string& suffix = "");
    ~Path() {}

    const char* getFile() { return m_file.c_str(); }
    const char* getObject();
    bool hasObject() { return !m_object.empty(); }

    std::unique_ptr<TH1> getHistogram();
    Filelist getFilelist();
private:
    void init(std::string& input);
    std::string m_file;
    std::string m_object;
};

}

#endif // RTK_PATH_H
