#include <iomanip>
#include <utility>
#include <stdexcept>
#include <boost/algorithm/string/join.hpp>
#include "TFile.h"
#include "TROOT.h"
#include "TLeaf.h"
#include "Filelist.h"
#include "Path.h"
#include "RTKException.h"
#include "docopt.h"
#include "common.h"
#include "ls.h"

namespace rtk {

bool isDir(TKey *key) {
    TClass *cl = gROOT->GetClass(key->GetClassName());
    return cl->InheritsFrom("TDirectory");
}

void lsTree(TTree* tree, bool longOutput, std::ostream& os) {
    if (longOutput) {
        size_t keylen = 0;
        std::vector<std::pair<std::string, std::string> > result;

        TObjArray* leaves = tree->GetListOfLeaves();
        for (auto *obj : *leaves) {
            TLeaf* leaf = (TLeaf*)obj;
            std::string name(leaf->GetName());
            keylen = std::max(keylen, name.length());
            result.push_back(make_pair(name, leaf->GetTypeName()));
        }

        for (auto &pair : result) {
            os
                << std::setw(keylen + 2)
                << std::left
                << pair.first
                << pair.second
                << "\n";
        }

        os << std::endl;
    } else {
        std::vector<std::string> list;
        TObjArray* leaves = tree->GetListOfLeaves();
        for (auto *leaf : *leaves)
            list.emplace_back(leaf->GetName());
        os << boost::algorithm::join(list, " \t") << std::endl;
    }
}

void lsDir(const TDirectory* dir, bool longOutput, bool dirOnly, std::ostream& os) {
    TIter next(dir->GetListOfKeys());

    if (longOutput) {
        size_t keylen = 0;
        std::vector<std::pair<std::string, std::string> > result;
        for (TKey *key = (TKey*) *next; (key = (TKey*) next()); ) {
            if (dirOnly && !isDir(key)) continue;
            keylen = std::max(keylen, std::string(key->GetName()).length());
            result.push_back(std::make_pair(key->GetName(), key->GetClassName()));
        }
        for (auto pair : result) {
            os
                << std::setw(keylen + 2)
                << std::left
                << pair.first
                << pair.second
                << "\n";
        }
        os << std::endl;

    } else {
        std::vector<std::string> list;
        for (TKey *key = (TKey*) *next; (key = (TKey*) next()); ) {
            if (dirOnly && !isDir(key)) continue;
            list.emplace_back(key->GetName());
        }
        os << boost::algorithm::join(list, " \t") << std::endl;
    }
}

int ls(const std::map<std::string, docopt::value> args, std::ostream& os) {
    try {
        Path path(args.at("<path>").asString());
        Filelist files(path.getFile());
        std::unique_ptr<TFile> file = files.at(0);

        bool longOutput = args.at("--long").asBool();
        bool dirOnly = args.at("--directory").asBool();

        if (path.hasObject()) {
            //if (file->FindKey(path.getObject()) == nullptr)
            //    throw RTKException("Key \"") << path.getObject()
            //        << "\" does not exist in file " << path.getFile() << ".";
            TObject* obj = file->Get(path.getObject());
            if (obj->InheritsFrom("TTree"))
                lsTree((TTree*)obj, longOutput, os);
            else if (obj->InheritsFrom("TDirectory"))
                lsDir((TDirectory*)obj, longOutput, dirOnly, os);
            else
                throw RTKException("Path must point to a TDirectory or TTree");
        } else {
            lsDir((TDirectory*)file.get(), longOutput, dirOnly, os);
        }
    } catch (std::exception &e) {
        std::cerr << "RTK List failed: " << e.what() << std::endl;
        return 1;
    }
}

}
