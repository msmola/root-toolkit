#ifndef RTK_RTKEXCEPTION_H
#define RTK_RTKEXCEPTION_H
#include <string>
#include <sstream>
#include <stdexcept>

#include <iostream>

namespace rtk {

class RTKException : public std::exception {
public:
    explicit RTKException()
        : m_msg()
    {}
    explicit RTKException(const char* message)
        : m_msg(message)
    {}
    explicit RTKException(const std::string& message)
        : m_msg(message)
    {}
    RTKException(const RTKException& other)
        : m_msg(other.m_msg)
    {}
    virtual ~RTKException() throw() {}

    virtual const char* what() const throw() {
        return m_msg.c_str();
    }

    template <class T>
    RTKException& operator<<(const T& msg) {
        m_msg += msg;
        return *this;
    }

private:
    std::string m_msg;
};

}

#endif // RTK_RTKEXCEPTION_H
