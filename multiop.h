#ifndef RTK_MULTIOP_H
#define RTK_MULTIOP_H

#include <iostream>
#include <sstream>
#include "docopt.h"


namespace rtk {
using ArgMap = std::map<std::string, docopt::value>;

void entries(const ArgMap args, std::ostream& os = std::cout);

}

#endif // RTK_MULTIOP_H
