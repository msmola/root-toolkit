#include <boost/algorithm/string.hpp>
#include "RTKException.h"
#include "Filelist.h"
#include "Path.h"
#include "common.h"

namespace rtk {

void Path::init(std::string& input) {
    if (DEBUG_ENABLED()) std::cerr << "Path init " << input << std::endl;
    boost::algorithm::trim(input);
    size_t split = input.rfind(".txt");
    if (split != std::string::npos) {
        split += 4;
    } else {
        split = input.rfind(".root");

        if (split == std::string::npos)
            throw RTKException("Path Error:\n")
                << "Paths must be of the form "
                << "\"path/to/rootfile(.root|.txt)[/path/to/TObject]\".\n"
                << "No \".root\" or \".txt\" file ending found.\n"
                << "Path: \"" << input << "\".";

        split += 5;
    }

    m_file = input.substr(0, split);

    if (split == input.length())
        return;

    if (input.at(split) != '/')
        throw RTKException("Path Error:\n")
            << "Paths must be of the form "
            << "\"path/to/rootfile(.root|.txt)[/path/to/TObject]\".\n"
            << "First character after filepath must be \"/\".";

    m_object = input.substr(split + 1);
}

Path::Path(std::string& input) {
    init(input);
}

Path::Path(const char* input) {
    std::string inpath(input);
    init(inpath);
}

Path::Path(const std::string& input, const std::string& prefix, const std::string& suffix) {
    std::string inpath(prefix + input + suffix);
    init(inpath);
}

const char* Path::getObject() {
    if (hasObject())
        return m_object.c_str();

    throw RTKException("Path Error:\n")
        << "Path does not specify Object.";
}

Filelist Path::getFilelist() {
    return Filelist(getFile());
}

std::unique_ptr<TH1> Path::getHistogram() {
    std::unique_ptr<TH1> histo;
    try {
        histo = Filelist(getFile()).hadd(getObject());
    } catch (RTKException& e) {
        throw RTKException("Path::getHistogram failed: ") << e.what();
    }
    return histo;
}

}
