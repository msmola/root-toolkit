import subprocess

def run(subcmd, *args, **kwargs):
    cmd = ['rtk', subcmd]
    for arg, val in kwargs.items():
        if val == True:
            cmd.append('--{}'.format(arg))
            continue
        
        if val == False:
            continue

        cmd.append('--{} {}'.format(arg, val))

    cmd.extend(args)

    result = subprocess.check_output(cmd).split('\n')
    result.pop()
    return result

def integral(*args, **kwargs):
    return run('integral', *args, **kwargs)

def fill(*args, **kwargs):
    return run('fill', *args, **kwargs)

def add(*args, **kwargs):
    return run('add', *args, **kwargs)

def sub(*args, **kwargs):
    return run('sub', *args, **kwargs)

def mul(*args, **kwargs):
    return run('mul', *args, **kwargs)
