#include <iostream>
#include <sstream>
#include <sys/stat.h>
#include "docopt.cpp/docopt.h"
#include "draw.h"
#include "treeop.h"
#include "test/common.h"
#include "common.h"
#include "usage.h"

using namespace rtk;
using ArgMap = std::map<std::string, docopt::value>;

std::string fillHistogram(const std::string& title = "default") {
    ArgMap fillargs = docopt::docopt(
            usage,
            {"fill", "jet_pt[0]", "test/testfile.root/nominal_Loose",
            "--title", title.c_str(), "--color", "2"},
            true
            );

    std::stringstream tmpbuf;
    fill(fillargs, tmpbuf);
    std::string tmpfile;
    tmpbuf >> tmpfile;
    return tmpfile;
}


int testFillAndDraw() {
    std::string tmpfile;
    TRY(tmpfile = fillHistogram());

    ArgMap drawargs = docopt::docopt(
            usage,
            {"draw", "--save", "test/fill_and_draw.pdf", "--batch", tmpfile.c_str()},
            true
            );

    //    for (auto const& arg : drawargs)
    //        std::cerr << arg.first << " = " << arg.second << std::endl;

    TRY(draw(drawargs));

    struct stat buffer;
    ASSERT(stat("test/fill_and_draw.pdf", &buffer) == 0);
    return 0;
};

int testFillAndDrawStack() {
    std::string tmpfile;
    TRY(tmpfile = fillHistogram());

    ArgMap drawargs = docopt::docopt(
            usage,
            {"draw", "--save", "test/fill_and_draw_only_stack.pdf", "--batch",
            "stack", tmpfile.c_str()},
            true
            );

    TRY(draw(drawargs));

    struct stat buffer;
    ASSERT(stat("test/fill_and_draw_only_stack.pdf", &buffer) == 0);
    

    drawargs = docopt::docopt(
            usage,
            {"draw", "--save", "test/fill_and_draw_both.pdf", "--batch",
            "--title", "Title", "--legend",
            tmpfile.c_str(), "stack", tmpfile.c_str()},
            true
            );

    TRY(draw(drawargs));
    ASSERT(stat("test/fill_and_draw_both.pdf", &buffer) == 0);
    return 0;
}

int testFillAndDrawRatio() {
    std::string tmpfile1, tmpfile2;
    TRY(tmpfile1 = fillHistogram("h1"));
    TRY(tmpfile2 = fillHistogram("h2"));

    ArgMap drawargs = docopt::docopt(
            usage,
            {"draw", "--save", "test/fill_and_draw_ratio.pdf", "--batch",
            "--ratio", "--legend", "--title", "This is a title", "--debug",
            "--atlas", "--preliminary", "--energy", "13 TeV", "--lumi", "36fb-1",
            tmpfile1.c_str(), "stack", tmpfile2.c_str()},
            true
            );

    TRY(draw(drawargs));
    struct stat buffer;
    ASSERT(stat("test/fill_and_draw_ratio.pdf", &buffer) == 0);

    drawargs = docopt::docopt(
            usage,
            {"draw", "--save", "test/fill_and_draw_ratio_log.pdf", "--batch",
            "--ratio", "--legend", "--title", "This is a title", "--debug",
            "--atlas", "--preliminary", "--energy", "13 TeV", "--lumi", "36fb-1",
            "--logy",
            tmpfile1.c_str(), "stack", tmpfile2.c_str()},
            true
            );

    TRY(draw(drawargs));
    return 0;
}

int main(int args, char** argv) {
    CHECK(testFillAndDraw);
    CHECK(testFillAndDrawStack);
    CHECK(testFillAndDrawRatio);
    return 0;
}

