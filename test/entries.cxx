#include <iostream>
#include <sstream>
#include <sys/stat.h>
#include "docopt.cpp/docopt.h"
#include "hmath.h"
#include "test/common.h"
#include "common.h"
#include "usage.h"

using namespace rtk;
using ArgMap = std::map<std::string, docopt::value>;

int testTreeEntries() {
    ArgMap args = docopt::docopt(
            usage,
            {"entries", "test/testfile.root/nominal_Loose"},
            true
            );

    std::stringstream ss;
    entries(args, ss);
    std::string e;
    ss >> e;
    ASSERT(std::stoi(e) == 2135);
    return 0;
}

int main(int argc, char** argv) {
    CHECK(testTreeEntries);
    return 0;
}
