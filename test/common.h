#ifndef RTK_TEST_COMMON_H
#define RTK_TEST_COMMON_H

#include <iostream>
#include <stdexcept>

#define CHECK( METHOD ) \
    std::cout << #METHOD << std::endl; \
    if ( METHOD() ) { \
        std::cerr << "Test failed." << std::endl; \
        return 1; \
    }

#define ASSERT( EXPR ) if (!(EXPR)) { \
    std::cerr << "Assertion failure:\n" << #EXPR << std::endl; \
    return 1; \
}

#define TRY( EXPR ) try { \
    EXPR; \
} catch (std::exception &e) { \
    std::cerr \
        << "Runtime Error in line " \
        << __LINE__ << ":\n" \
        << #EXPR << "\n" \
        << e.what() \
        << std::endl; \
    return 1; \
}


#endif // RTK_TEST_COMMON_H
