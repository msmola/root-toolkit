#include <vector>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include "TClass.h"
#include "TFile.h"
#include "RTKException.h"
#include "Path.h"
#include "common.h"

namespace rtk {

using ArgMap = std::map<std::string, docopt::value>;

std::string hash(const std::string &input, const size_t len) {
    std::ostringstream oss;
    oss << std::hex << std::hash<std::string>{}(input);
    return len ? oss.str().substr(0, len) : oss.str();
}

std::string hash(const char* input, const size_t len) {
    return hash(std::string(input), len);
}

std::string hash(const std::map<std::string, docopt::value> args, const size_t len) {
    std::ostringstream oss;
    for (auto const& arg: args) {
        if (arg.first == "--no-cache") continue;
        oss << arg.first << "=" << arg.second << "&";
    }
    
    return hash(oss.str(), len);
}

std::string cache(const char* hash, TH1* histo) {
    std::ostringstream ident;
    ident << "/tmp/rtk_" << hash << ".root";

    auto* file = new TFile(ident.str().c_str(), "RECREATE");
    if (!file->IsOpen())
        throw RTKException("Failed to recreate file ") << ident.str() << ".";
        
    histo->SetDirectory(file);
    histo->Write();

    ident << "/" << histo->GetName();
    file->Close();
    return ident.str();
}

std::string write(TH1* histo, const std::string& target, bool recreate) {
    Path targetpath(target);

    auto* file = new TFile(
            targetpath.getFile(),
            recreate ? "RECREATE" : "UPDATE"
            );
    if (!file->IsOpen())
        throw RTKException("Failed to open target file: ")
            << targetpath.getFile() << ".";

    TH1* clone = (TH1*)histo->Clone();

    std::string path(targetpath.getFile());
    path += "/";

    if (targetpath.hasObject()) {
        clone->SetName(targetpath.getObject());
        path += targetpath.getObject();
    } else {
        path += clone->GetName();
    }

    file->Write();
    file->Close();
    return path;
}

std::string pipeArg(const docopt::value &value) {
    if (value) return value.asString();

    std::string path;
    std::cin >> path;
    return path;
}

Int_t getPalette(const std::string& name) {
    std::map<std::string, Int_t> palettes = { 
        {"kDeepSea", 51},          {"kGreyScale", 52},    {"kDarkBodyRadiator", 53},
        {"kBlueYellow", 54},       {"kRainBow", 55},      {"kInvertedDarkBodyRadiator", 56},
        {"kBird", 57},             {"kCubehelix", 58},    {"kGreenRedViolet", 59},
        {"kBlueRedYellow", 60},    {"kOcean", 61},        {"kColorPrintableOnGrey", 62},
        {"kAlpine", 63},           {"kAquamarine", 64},   {"kArmy", 65},
        {"kAtlantic", 66},         {"kAurora", 67},       {"kAvocado", 68},
        {"kBeach", 69},            {"kBlackBody", 70},    {"kBlueGreenYellow", 71},
        {"kBrownCyan", 72},        {"kCMYK", 73},         {"kCandy", 74},
        {"kCherry", 75},           {"kCoffee", 76},       {"kDarkRainBow", 77},
        {"kDarkTerrain", 78},      {"kFall", 79},         {"kFruitPunch", 80},
        {"kFuchsia", 81},          {"kGreyYellow", 82},   {"kGreenBrownTerrain", 83},
        {"kGreenPink", 84},        {"kIsland", 85},       {"kLake", 86},
        {"kLightTemperature", 87}, {"kLightTerrain", 88}, {"kMint", 89},
        {"kNeon", 90},             {"kPastel", 91},       {"kPearl", 92},
        {"kPigeon", 93},           {"kPlum", 94},         {"kRedBlue", 95},
        {"kRose", 96},             {"kRust", 97},         {"kSandyTerrain", 98},
        {"kSienna", 99},           {"kSolar", 100},       {"kSouthWest", 101},
        {"kStarryNight", 102},     {"kSunset", 103},      {"kTemperatureMap", 104},
        {"kThermometer", 105},     {"kValentine", 106},   {"kVisibleSpectrum", 107},
        {"kWaterMelon", 108},      {"kCool", 109},        {"kCopper", 110},
        {"kGistEarth", 111},       {"kViridis", 112},     {"kCividis", 113}
    };
    try {
        return palettes.at(name);
    } catch (std::out_of_range &e) {
        return std::stol(name);
    }
}


Color_t getColor(const std::string& name) {
    std::map<std::string, Color_t> colors = {
        {"kWhite", kWhite},     {"kBlack", kBlack},     {"kGray", kGray},
        {"kRed", kRed},         {"kGreen", kGreen},     {"kBlue", kBlue},
        {"kYellow", kYellow},   {"kMagenta", kMagenta}, {"kCyan", kCyan},
        {"kOrange", kOrange},   {"kSpring", kSpring},   {"kTeal", kTeal},
        {"kAzure", kAzure},     {"kViolet", kViolet},   {"kPink", kPink}
    };
    try {
        return colors.at(name);
    } catch (std::out_of_range &e) {
        return std::stol(name);
    }
}

const docopt::value& getArg(const char* name, const std::map<std::string, docopt::value>& args) {
    try {
        return args.at(name);
    } catch (std::out_of_range &e) {
        throw RTKException(e.what())
            << " \"" << name << "\"";
    }
}

const std::string& getStringArg(const char* name, const std::map<std::string, docopt::value>& args) {
    auto& arg = getArg(name, args);
    if (arg.isString())
        return arg.asString();
    else
        throw RTKException("Argument ") << name << " is not of type String";
}

const double getDoubleArg(const char* name, const ArgMap& args) {
    auto& arg = getStringArg(name, args);
    return std::stod(arg);
}

bool DEBUG = false;

void SET_DEBUG(const bool enabled) {
    DEBUG = enabled;
}

bool DEBUG_ENABLED() {
    return DEBUG;
}
}
