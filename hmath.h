#ifndef RTK_HMATH_H
#define RTK_HMATH_H

#include <iostream>
#include <sstream>
#include "docopt.h"

namespace rtk {

using ArgMap = std::map<std::string, docopt::value>;

void add(const ArgMap& args, std::ostream& os = std::cout);
void sub(const ArgMap& args, std::ostream& os = std::cout);
void mul(const ArgMap& args, std::ostream& os = std::cout);
void div(const ArgMap& args, std::ostream& os = std::cout);
void scale(const ArgMap& args, std::ostream& os = std::cout);
void set(const ArgMap& args, std::ostream& os = std::cout);

void entries(const ArgMap& args, std::ostream& os = std::cout);
void binning(const ArgMap& args, std::ostream& os = std::cout);
void rebin(const ArgMap& args, std::ostream& os = std::cout);
void integral(const ArgMap& args, std::ostream& os = std::cout);
void sumweights(const ArgMap& args, std::ostream& os = std::cout);
void projection(const ArgMap& args, std::ostream& os = std::cout);
void min(const ArgMap& args, std::ostream& os = std::cout);
void max(const ArgMap& args, std::ostream& os = std::cout);
void avg(const ArgMap& args, std::ostream& os = std::cout);

}

#endif // RTK_HMATH_H
