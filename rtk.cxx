#include <iostream>
#include "docopt.h"
#include "ls.h"
#include "draw.h"
#include "print.h"
#include "treeop.h"
#include "save.h"
#include "hmath.h"
#include "usage.h"
#include "common.h"
#include "RTKException.h"

using namespace rtk;


int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
        = docopt::docopt(usage, { argv + 1, argv + argc }, true, "0.0.1");

    SET_DEBUG(args.at("--debug").asBool());

    if (DEBUG_ENABLED()) {
        for (auto const& arg : args)
            std::cerr << arg.first << " = " << arg.second << std::endl;
    }
    
    try {
        if (args.at("ls").asBool()) {
            ls(args);
        } else if (args.at("draw").asBool()) {
            draw(args, std::cout);
        } else if (args.at("print").asBool()) {
            print(args, std::cout);
        } else if (args.at("fill").asBool()) {
            fill(args);
        } else if (args.at("add").asBool()) {
            add(args);
        } else if (args.at("sub").asBool()) {
            sub(args);
        } else if (args.at("mul").asBool()) {
            mul(args);
        } else if (args.at("div").asBool()) {
            div(args);
        } else if (args.at("scale").asBool()) {
            scale(args);
        } else if (args.at("save").asBool()) {
            save(args);
        } else if (args.at("sum").asBool()) {
            sum(args);
        } else if (args.at("entry").asBool()) {
            entry(args);
        } else if (args.at("entries").asBool()) {
            entries(args);
        } else if (args.at("integral").asBool()) {
            integral(args);
        } else if (args.at("sumweights").asBool()) {
            sumweights(args);
        } else if (args.at("projection").asBool()) {
            projection(args);
        } else if (args.at("set").asBool()) {
            set(args);
        } else if (args.at("binning").asBool()) {
            binning(args);
        } else if (args.at("rebin").asBool()) {
            rebin(args);
        } else if (args.at("min").asBool()) {
            min(args);
        } else if (args.at("max").asBool()) {
            max(args);
        } else if (args.at("avg").asBool()) {
            avg(args);
        } else {
            throw RTKException("No command specified.");
        }
        return 0;
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}
