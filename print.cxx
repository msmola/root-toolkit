#include <iostream>
#include <stdexcept>
#include <iomanip>
#include <memory>
#include "print.h"
#include "Filelist.h"
#include "Path.h"
#include "common.h"

namespace rtk {

int print(std::map<std::string, docopt::value> args, std::ostream& os) {
    try {
        Path path(pipeArg(args.at("<path>")));
        Filelist files = Filelist(path.getFile());

        std::unique_ptr<TH1> histo = files.hadd(path.getObject(), getArg("--debug", args).asBool());

        if (histo->InheritsFrom("TH2")) {
            return print2((TH2*)histo.get(), os);
        }

        return print1(histo.get(), os);
        
    } catch (std::exception &e) {
        std::cerr << "RTK Print Error: " << e.what() << std::endl;
        return 1;
    }
}

int print1(TH1* histo, std::ostream& os) {
    Int_t nx = histo->GetNbinsX();

    os
        << std::setw(100)
        << histo->GetName()
        << std::endl;

    os
        << "  Entries: "
        << std::setw(12) 
        << std::setprecision(3) 
        << histo->GetEffectiveEntries() 
        << "\n  Mean: "
        << std::setw(14)
        << std::setprecision(3)
        << histo->GetMean() 
        << "\n"
        << std::endl;

    os
        << std::setw(10)
        << std::setprecision(3)
        << histo->GetBinContent(0)
        << " :";


    for (Int_t x = 1; x <= nx; x++) {
        os 
            << std::setw(10)
            << std::setprecision(3)
            << histo->GetBinContent(x);
    }

    os
        << " :"
        << std::setw(10)
        << std::setprecision(3)
        << histo->GetBinContent(nx + 1)
        << "\n";

    TAxis* xAxis = histo->GetXaxis();
    if (xAxis->IsAlphanumeric()) {
        os
            << std::setw(10)
            << "UFLOW"
            << " :";

        for (Int_t x = 1; x <= nx; x++)
            os << std::setw(10) << std::string(xAxis->GetBinLabel(x)).substr(0, 8);

        os
            << " :"
            << std::setw(10)
            << "OFLOW";
    } else {
        os
            << std::setw(7)
            << ""
            << std::setw(10)
            << std::setprecision(3)
            << xAxis->GetBinLowEdge(1);

        for (Int_t x = 1; x <= nx; x++)
            os << std::setw(10) << xAxis->GetBinUpEdge(x);

        os
            << std::setw(7)
            << "";
    }

    os << std::endl;

    return 0;
}

int print2(TH2* histo, std::ostream& os) {
    Int_t nx = histo->GetNbinsX();
    Int_t ny = histo->GetNbinsY();

    TAxis* xAxis = histo->GetXaxis();
    TAxis* yAxis = histo->GetYaxis();

    // name
    os
        << std::setw(100)
        << histo->GetName()
        << std::endl;

    // stats
    os
        << "  Entries: "
        << std::setw(12) 
        << std::setprecision(3) 
        << histo->GetEffectiveEntries() 
        << "\n  Mean: "
        << std::setw(15)
        << histo->GetMean() 
        << "\n"
        << std::endl;

    // top left overflow
    if (yAxis->IsAlphanumeric()) {
        os
            << std::left
            << std::setw(10)
            << "OFLOW"
            << std::right
            << " |";
    } else {
        os
            << std::setw(10)
            << std::setprecision(3)
            << yAxis->GetBinUpEdge(ny)
            << "_|";
    }

    os
        << std::setw(10)
        << std::setprecision(3)
        << histo->GetBinContent(0, ny + 1)
        << "  ";

    // top overflow
    for (Int_t x = 1; x <= nx; x++) {
        os 
            << std::setw(10)
            << std::setprecision(3)
            << histo->GetBinContent(x, ny + 1);
    }

    // top right overflow
    os
        << "  "
        << std::setw(10)
        << std::setprecision(3)
        << histo->GetBinContent(nx + 1, ny + 1)
        << "\n";

    // separator line
    os
        << std::setw(12)
        << "|"
        << std::setw(11)
        << ""
        << std::setw((nx * 10) + 2)
        << std::setfill('.')
        << ""
        << std::setfill(' ')
        << std::setw(11)
        << ""
        << "\n";

    for (Int_t y = ny; y > 0; y--) {
        // left label
        if (yAxis->IsAlphanumeric()) {
            os
                << std::setw(10)
                << std::left
                << std::string(yAxis->GetBinLabel(y)).substr(0, 10)
                << std::right
                << " |";
        } else {
            os
                << std::setw(10)
                << std::setprecision(3)
                << yAxis->GetBinLowEdge(y)
                << "_|";
        }


        // left underflow
        os
            << std::setw(10)
            << std::setprecision(3)
            << histo->GetBinContent(0, y)
            << " :";

        // values
        for (Int_t x = 1; x <= nx; x++) {
            os
                << std::setw(10)
                << std::setprecision(3)
                << histo->GetBinContent(x, y);
        }

        // right overflow
        os
            << " :"
            << std::setw(10)
            << std::setprecision(3)
            << histo->GetBinContent(nx + 1, y)
            << "\n";
    }

    // separator line
    os
        << std::setw(12)
        << "|"
        << std::setw(11)
        << ""
        << std::setw((nx * 10) + 2)
        << std::setfill('.')
        << ""
        << std::setfill(' ')
        << std::setw(11)
        << ""
        << "\n";



    // bottom left label
    if (yAxis->IsAlphanumeric()) {
        os
            << std::setw(10)
            << std::left
            << "UFLOW"
            << std::right
            << " |";
    } else {
        os
            << std::setw(12)
            << "|";
    }

    // bottom left underflow
    os
        << std::setw(10)
        << std::setprecision(3)
        << histo->GetBinContent(0, 0)
        << "  ";

    // bottom underflow
    for (Int_t x = 1; x <= nx; x++) {
        os
            << std::setw(10)
            << std::setprecision(3)
            << histo->GetBinContent(x, 0);
    }

    // bottom right overflow
    os
        << "  "
        << std::setw(10)
        << std::setprecision(3)
        << histo->GetBinContent(nx + 1, 0)
        << "\n";

    // separator line
    os
        << std::setw(11)
        << ""
        << std::setw((nx * 10) + 25)
        << std::setfill('-')
        << ""
        << std::setfill(' ')
        << "\n";

    // bottom left space
    os
        << std::setw(12)
        << "";

    // bottom labels
    if (xAxis->IsAlphanumeric()) {
        os
            << std::left
            << std::setw(10)
            << "UFLOW"
            << "  ";

        for (Int_t x = 1; x <= nx; x++)
            os
                << std::setw(10)
                << xAxis->GetBinLabel(x);

        os
            << "  "
            << std::setw(10)
            << "OFLOW";
    } else {
        os 
            << std::setw(7)
            << ""
            << std::setw(10)
            << std::setprecision(3)
            << xAxis->GetBinLowEdge(1);

        for (Int_t x = 1; x <= nx; x++)
            os
                << std::setw(10)
                << std::setprecision(3)
                << xAxis->GetBinUpEdge(x);

        os
            << std::setw(7)
            << "";
    }

    os << std::endl;

    return 0;
}
 

}
