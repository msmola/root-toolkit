#ifndef RTK_FILELIST_H
#define RTK_FILELIST_H

#include <memory>
#include "TTree.h"
#include "TH1.h"
#include "TFile.h"
#include "TChain.h"

namespace rtk {

class Filelist {
public:
    Filelist(const char *input);
    Filelist(const std::string& input);
    ~Filelist();

    void add(const std::string input);

    std::unique_ptr<TChain> getChain(const char *name);
    std::unique_ptr<TFile> at(const size_t index, bool debug = false);
    std::unique_ptr<TH1> hadd(const char *path, bool debug = false);
    std::unique_ptr<TObject> get(const char *path, bool debug = false);

private:
    std::vector<std::string> m_filenames;
};

}
#endif // RTK_FILELIST_H
