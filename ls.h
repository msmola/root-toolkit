#ifndef RTK_LS_H
#define RTK_LS_H

#include <iostream>
#include <sstream>
#include "TTree.h"
#include "TDirectory.h"
#include "TKey.h"
#include "docopt.h"

namespace rtk {

bool isDir(TKey *key);

int ls(const std::map<std::string, docopt::value> args, std::ostream& os = std::cout);

//int lsTree(
//        TTree* tree,
//        bool longOutput = false,
//        std::ostream& os = std::cout
//        );
//
//int lsDir(
//        const TDirectory* dir,
//        bool longOutput = false,
//        bool dirOnly = false,
//        std::ostream& os = std::cout
//        );


}

#endif // RTK_LS_H
