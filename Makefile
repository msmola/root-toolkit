FLAGS = -ftemplate-depth-30

FLAGS1 = -O3 -D DEFECT_NO_EXPLICIT_QUALIFICATION
FLAGS2 = -Og -D DEFECT_NO_EXPLICIT_QUALIFICATION


SPECIALFLAGS=-O -fPIC
ROOTCFLAGS=$(shell root-config --cflags)
ROOTLIBS=$(shell root-config --libs)

CFLAGS = $(SPECIALFLAGS) -I. 
LFLAGS = -O -L/sw/lib 

RCXX=$(CFLAGS) $(ROOTCFLAGS) -Idocopt.cpp -I/usr/include -std=c++1y
RLXX=$(LFLAGS) $(ROOTLIBS) -Ldocopt.cpp -lMinuit -lm

OCOMMON=build/Filelist.o build/Path.o build/common.o build/docopt.o
HCOMMON=RTKException.h Filelist.h Path.h common.h docopt.cpp/docopt.h usage.h

OALL=build/rtk.o $(OCOMMON) build/ls.o build/draw.o build/print.o build/treeop.o \
    build/save.o build/hmath.o

all : bin

bin : bin/rtk

bin/rtk : $(OALL) usage.h
	mkdir -p bin
	g++ -O3 -o bin/rtk $(OALL) $(RLXX)

build : $(OALL)

build/rtk.o : rtk.cxx ls.h docopt.cpp/docopt.h
	mkdir -p build
	g++ $(FLAGS1) -o build/rtk.o -c rtk.cxx $(RCXX)

build/docopt.o : docopt.cpp/docopt.cpp
	mkdir -p build
	g++ -O3 -o build/docopt.o -std=c++11 -c docopt.cpp/docopt.cpp

build/common.o : common.cxx Path.h
	mkdir -p build
	g++ $(FLAGS1) -o build/common.o -c common.cxx $(RCXX)

build/Filelist.o : Filelist.cxx RTKException.h
	mkdir -p build
	g++ $(FLAGS1) -o build/Filelist.o -c Filelist.cxx $(RCXX)

build/Path.o : Path.cxx RTKException.h Filelist.h common.h
	mkdir -p build
	g++ $(FLAGS1) -o build/Path.o -c Path.cxx $(RCXX)

build/ls.o : ls.cxx ls.h $(HCOMMON)
	mkdir -p build
	g++ $(FLAGS1) -o build/ls.o -c ls.cxx $(RCXX)

build/draw.o : draw.cxx draw.h $(HCOMMON)
	mkdir -p build
	g++ $(FLAGS1) -o build/draw.o -c draw.cxx $(RCXX) 

build/print.o : print.cxx print.h $(HCOMMON)
	mkdir -p build
	g++ $(FLAGS1) -o build/print.o -c print.cxx $(RCXX)

build/treeop.o : treeop.cxx treeop.h $(HCOMMON)
	mkdir -p build
	g++ $(FLAGS1) -o build/treeop.o -c treeop.cxx $(RCXX)

build/save.o : save.cxx save.h $(HCOMMON)
	mkdir -p build
	g++ $(FLAGS1) -o build/save.o -c save.cxx $(RCXX)

build/hmath.o : hmath.cxx hmath.h $(HCOMMON)
	mkdir -p build
	g++ $(FLAGS1) -o build/hmath.o -c hmath.cxx $(RCXX)


HTEST=docopt.cpp/docopt.h test/common.h common.h usage.h

test/build/RTKException.o : test/RTKException.cxx RTKException.h test/common.h
	mkdir -p test/build
	g++ $(FLAGS2) -o test/build/RTKException.o -c test/RTKException.cxx $(RCXX)

test/build/Filelist.o : test/Filelist.cxx Filelist.h test/common.h
	mkdir -p test/build
	g++ $(FLAGS2) -o test/build/Filelist.o -c test/Filelist.cxx $(RCXX)

test/build/ls.o : test/ls.cxx ls.h $(HTEST)
	mkdir -p test/build
	g++ $(FLAGS2) -o test/build/ls.o -c test/ls.cxx $(RCXX)

test/build/draw.o : test/draw.cxx draw.h $(HTEST)
	mkdir -p test/build
	g++ $(FLAGS2) -o test/build/draw.o -c test/draw.cxx $(RCXX)

test/build/integral.o : test/integral.cxx hmath.h $(HTEST)
	mkdir -p test/build
	g++ $(FLAGS2) -o test/build/integral.o -c test/integral.cxx $(RCXX)

test/build/add.o : test/add.cxx hmath.h $(HTEST)
	mkdir -p test/build
	g++ $(FLAGS2) -o test/build/add.o -c test/add.cxx $(RCXX)

test/build/entries.o : test/entries.cxx hmath.h $(HTEST)
	mkdir -p test/build
	g++ $(FLAGS2) -o test/build/entries.o -c test/entries.cxx $(RCXX)

test/bin/RTKException : test/build/RTKException.o
	mkdir -p test/bin
	g++ -o test/bin/RTKException test/build/RTKException.o $(RLXX)

test/bin/Filelist : test/build/Filelist.o build/Filelist.o 
	mkdir -p test/bin
	g++ -o test/bin/Filelist test/build/Filelist.o build/Filelist.o $(RLXX)

test/bin/ls : test/build/ls.o build/ls.o $(OCOMMON)
	mkdir -p test/bin
	g++ -o test/bin/ls test/build/ls.o build/ls.o $(OCOMMON) $(RLXX)

test/bin/draw : test/build/draw.o build/draw.o build/treeop.o build/common.o $(OCOMMON)
	mkdir -p test/bin
	g++ -o test/bin/draw test/build/draw.o build/draw.o build/treeop.o $(OCOMMON) $(RLXX)

test/bin/integral : test/build/integral.o build/hmath.o build/common.o $(OCOMMON)
	mkdir -p test/bin
	g++ -o test/bin/integral test/build/integral.o build/hmath.o $(OCOMMON) $(RLXX)

test/bin/add : test/build/add.o build/hmath.o build/common.o $(OCOMMON)
	mkdir -p test/bin
	g++ -o test/bin/add test/build/add.o build/hmath.o $(OCOMMON) $(RLXX)

test/bin/entries : test/build/entries.o build/hmath.o build/common.o $(OCOMMON)
	mkdir -p test/bin
	g++ -o test/bin/entries test/build/entries.o build/hmath.o $(OCOMMON) $(RLXX)

test/RTKException : test/bin/RTKException
	./test/bin/RTKException

test/Filelist : test/bin/Filelist
	./test/bin/Filelist

test/ls : test/bin/ls
	./test/bin/ls

test/draw : test/bin/draw
	./test/bin/draw

test/integral : test/bin/integral
	./test/bin/integral

test/add : test/bin/add
	./test/bin/add

test/entries : test/bin/entries
	./test/bin/entries

test : test/RTKException test/Filelist test/ls test/draw test/integral test/add \
    test/entries

clean :
	rm -rf build bin test/build test/bin
	rm -rf test/*.pdf
