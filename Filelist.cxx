#include <fstream>
#include <vector>
#include <boost/algorithm/string.hpp>
#include "Filelist.h"
#include "RTKException.h"

namespace rtk {

Filelist::Filelist(const char* input) {
    add(input);
}

Filelist::Filelist(const std::string& input) {
    add(input);
}

void Filelist::add(const std::string input) {
    if (boost::ends_with(input, ".root")) {
        m_filenames.push_back(input);
        return;
    }

    if (!boost::ends_with(input, ".txt"))
        throw RTKException(
                "Input must be either single root file or list of root-files in .txt format"
                );

    std::ifstream infile(input.c_str());
    for (std::string line; getline(infile, line); ) {
        boost::trim(line);
        if (line.empty()) continue;
        m_filenames.push_back(line);
    }

    if (m_filenames.empty())
        throw RTKException("Input list is empty");
}

Filelist::~Filelist() { }

std::unique_ptr<TFile> Filelist::at(const size_t index, bool debug) {
    if (index >= m_filenames.size()) {
        throw RTKException()
            << "Out of bounds error: Trying to access file "
            << index
            << " but this filelist only has "
            << m_filenames.size()
            << " files.";
    }
    if (debug) std::cerr << "Open " << m_filenames.at(index) << std::endl;

    std::unique_ptr<TFile> file(TFile::Open(m_filenames.at(index).c_str()));

    if (!file->IsOpen())
        throw RTKException("Failed to open TFile ")
            << m_filenames.at(index)
            << ".";
    return file;
}

std::unique_ptr<TH1> Filelist::hadd(const char *path, bool debug) {
    std::unique_ptr<TFile> first = at(0, debug);
    //if (first->FindKey(path) == nullptr)
    //    throw RTKException("Key \"") << path << "\" does not exist in file "
    //        << m_filenames.at(0) << ".";
    std::unique_ptr<TH1> histo((TH1*)first->Get(path));
    histo->SetDirectory(0);
    if (!histo.get())
        throw RTKException("Failed to get \"") << path << "\" from file.";
    histo->Sumw2();
    for (size_t i = 1; i < m_filenames.size(); ++i) {
        std::unique_ptr<TFile> file = at(i, debug);
        //if (file->FindKey(path) == nullptr)
        //    throw RTKException("Key \"") << path << "\" does not exist in file "
        //        << m_filenames.at(0) << ".";
        TH1* next = (TH1*)file->Get(path);
        if (!next)
            throw RTKException("Failed to get \"")
                << path
                << "\" from file "
                << m_filenames.at(i)
                << ".";
        histo->Add(next);
    }
    return histo;
}

std::unique_ptr<TObject> Filelist::get(const char *path, bool debug) {
    // get object from first file
    std::unique_ptr<TFile> first = at(0, debug);
    TObject* obj = first->Get(path);
    if (!obj)
        throw RTKException("Failed to get \"") << path << "\" from file.";

    // check compatible types
    if (obj->InheritsFrom("TH1")) {
        std::unique_ptr<TH1> histo((TH1*)obj);
        histo->SetDirectory(0);
        histo->Sumw2();
        for (size_t i = 1; i < m_filenames.size(); ++i) {
            std::unique_ptr<TFile> file = at(i, debug);
            TH1* next = (TH1*)file->Get(path);
            if (!next)
                throw RTKException("Failed to get \"")
                    << path
                    << "\" from file "
                    << m_filenames.at(i)
                    << ".";
            histo->Add(next);
        }
        return histo;
    } else if (obj->InheritsFrom("TTree")) {
        first->Close();
        return getChain(path);
    }

    throw RTKException("Type of object at \"") << path << "\" is not supported yet."; 
}

std::unique_ptr<TChain> Filelist::getChain(const char *path) {
    // create TChain
    auto chain = std::make_unique<TChain>(path);

    // add files
    for (auto filename : m_filenames)
        chain->Add(filename.c_str());

    return chain;
}

}
