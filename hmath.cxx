#include <stdexcept>
#include <string>
#include <boost/algorithm/string.hpp>
#include "TH2.h"
#include "Filelist.h"
#include "common.h"
#include "Path.h"
#include "hmath.h"
#include "RTKException.h"

namespace rtk {

using ArgMap = std::map<std::string, docopt::value>;

void add(const ArgMap& args, std::ostream& os) {
    try {
        auto prefix = getStringArg("--prefix", args);
        auto suffix = getStringArg("--suffix", args);

        std::unique_ptr<TH1> histo = Path(
                pipeArg(getArg("<path>", args).asString()),
                prefix,
                suffix
                )
            .getHistogram();

        if (getArg("--title", args))
            histo->SetTitle(args.at("--title").asString().c_str());

        for (auto &otherpath : args.at("<other>").asStringList()) {
            std::unique_ptr<TH1> other = Path(otherpath, prefix, suffix).getHistogram();
            histo->Add(other.get());
        }

        if (args.at("--color")) {
            Color_t color = args.at("--color").isLong()
                ? args.at("--color").asLong()
                : getColor(args.at("--color").asString());
            histo->SetLineColor(color);
            histo->SetFillColor(color);
            histo->SetMarkerColor(color);
        }

        for (auto& outpath : getArg("--save", args).asStringList())
            os << write(histo.get(), outpath, getArg("--recreate", args).asBool()) << std::endl;
        if (args.at("--save").asStringList().empty())
            os << cache(hash(args, 8).c_str(), histo.release()) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Add failed: ") << e.what();
    }
}

void sub(const ArgMap& args, std::ostream& os) {
    try {
        std::unique_ptr<TH1> histo = Path(pipeArg(args.at("<path>").asString()))
            .getHistogram();

        if (args.at("--title"))
            histo->SetTitle(args.at("--title").asString().c_str());

        for (auto &otherpath : args.at("<other>").asStringList()) {
            std::unique_ptr<TH1> other = Path(otherpath).getHistogram();
            histo->Add(other.get(), -1);
        }

        if (args.at("--color")) {
            Color_t color = args.at("--color").isLong()
                ? args.at("--color").asLong()
                : getColor(args.at("--color").asString());
            histo->SetLineColor(color);
            histo->SetFillColor(color);
            histo->SetMarkerColor(color);
        }

        for (auto& outpath : getArg("--save", args).asStringList())
            os << write(histo.get(), outpath, getArg("--recreate", args).asBool()) << std::endl;
        if (args.at("--save").asStringList().empty())
            os << cache(hash(args, 8).c_str(), histo.release()) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Subtract failed: ") << e.what();
    }
}

void mul(const ArgMap& args, std::ostream& os) {
    try {
        std::unique_ptr<TH1> histo = Path(pipeArg(args.at("<path>").asString())).getHistogram();
        if (args.at("--title"))
            histo->SetTitle(args.at("--title").asString().c_str());

        for (auto &otherpath : args.at("<other>").asStringList()) {
            std::unique_ptr<TH1> other = Path(otherpath).getHistogram();
            histo->Multiply(other.get());
        }

        if (args.at("--color")) {
            Color_t color = args.at("--color").isLong()
                ? args.at("--color").asLong()
                : getColor(args.at("--color").asString());
            histo->SetLineColor(color);
            histo->SetFillColor(color);
            histo->SetMarkerColor(color);
        }

        for (auto& outpath : getArg("--save", args).asStringList())
            os << write(histo.get(), outpath, getArg("--recreate", args).asBool()) << std::endl;
        if (args.at("--save").asStringList().empty())
            os << cache(hash(args, 8).c_str(), histo.release()) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Multiply failed: ") << e.what();
    }
}

void div(const ArgMap& args, std::ostream& os) {
    try {
        std::unique_ptr<TH1> histo = Path(pipeArg(args.at("<path>").asString())).getHistogram();
        if (args.at("--title"))
            histo->SetTitle(args.at("--title").asString().c_str());

        for (auto &otherpath : args.at("<other>").asStringList()) {
            std::unique_ptr<TH1> other = Path(otherpath).getHistogram();
            histo->Divide(other.get());
        }

        if (args.at("--color")) {
            Color_t color = args.at("--color").isLong()
                ? args.at("--color").asLong()
                : getColor(args.at("--color").asString());
            histo->SetLineColor(color);
            histo->SetFillColor(color);
            histo->SetMarkerColor(color);
        }

        for (auto& outpath : getArg("--save", args).asStringList())
            os << write(histo.get(), outpath, getArg("--recreate", args).asBool()) << std::endl;
        if (args.at("--save").asStringList().empty())
            os << cache(hash(args, 8).c_str(), histo.release()) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Divide failed: ") << e.what();
    }
}

void scale(const ArgMap& args, std::ostream& os) {
    try {
        auto prefix = getStringArg("--prefix", args);
        auto suffix = getStringArg("--suffix", args);
        std::unique_ptr<TH1> histo = Path(
                pipeArg(getArg("<path>", args)),
                prefix,
                suffix
                ).getHistogram();

        for (auto &o : getArg("<more>", args).asStringList()) {
            std::unique_ptr<TH1> other = Path(o, prefix, suffix).getHistogram();
            histo->Add(other.get());
        }

        double factor = getDoubleArg("<factor>", args);
        for (auto &mul : getArg("--multiply-by", args).asStringList())
            factor *= std::stod(mul);
        for (auto &div : getArg("--divide-by", args).asStringList())
            factor /= std::stod(div);

        histo->Scale(factor);

        for (auto& outpath : getArg("--save", args).asStringList())
            os << write(histo.get(), outpath, getArg("--recreate", args).asBool()) << std::endl;
        if (args.at("--save").asStringList().empty())
            os << cache(hash(args, 8).c_str(), histo.release()) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Scale failed: ") << e.what();
    }
}

double getObjectEntries(TObject* obj, const ArgMap& args) {
        if (obj->InheritsFrom("TTree")) {
            auto tree = (TTree*)obj;
            if (getArg("--fast", args).asBool())
                return tree->GetEntriesFast();
            else if (getArg("--selection", args))
                return tree->GetEntries(args.at("--selection").asString().c_str());
            else 
                return tree->GetEntries();
        } else if (obj->InheritsFrom("TH1")) {
            auto histo = (TH1*)obj;
            if (getArg("--effective", args).asBool())
                return histo->GetEffectiveEntries();
            else
                return histo->GetEffectiveEntries();
        } else {
            throw RTKException("Cannot get entries for object of type ")
                << obj->ClassName();
        }
}

void entries(const ArgMap& args, std::ostream& os) {
    try {
        auto prefix = getStringArg("--prefix", args);
        auto suffix = getStringArg("--suffix", args);
        Path path(pipeArg(getArg("<path>", args)), prefix, suffix);
        Filelist files(path.getFile());

        double e = getObjectEntries(files.get(path.getObject()).get(), args);

        for (auto &o : getArg("<more>", args).asStringList()) {
            Path morepath(o, prefix, suffix);
            Filelist morefiles(path.getFile());
            e += getObjectEntries(morefiles.get(path.getObject()).get(), args);
        }

        os << e << std::endl;

    } catch (std::exception &e) {
        throw RTKException("RTK Entries failed: ") << e.what();
    }
}

void integral(const ArgMap& args, std::ostream& os) {
    try {
        auto prefix = getStringArg("--prefix", args);
        auto suffix = getStringArg("--suffix", args);
        Path path(
                pipeArg(getArg("<path>", args)),
                prefix,
                suffix
                );
        std::unique_ptr<TH1> histo = path.getHistogram();

        for (auto &o : args.at("<more>").asStringList()) {
            std::unique_ptr<TH1> other = Path(o, prefix, suffix).getHistogram();
            histo->Add(other.get());
        }

        Int_t min = args.at("--inclusive").asBool() ? 1 : 0;
        Int_t max = args.at("--inclusive").asBool() ? histo->GetNbinsX() + 1 : histo->GetNbinsX();

        if (args.at("--pmerror").asBool()) {
            Double_t error;
            Double_t value = histo->IntegralAndError(min, max, error);
            os << value << "+-" << error << std::endl;
        } else if (args.at("--nlerror").asBool()) {
            Double_t error;
            Double_t value = histo->IntegralAndError(min, max, error);
            os << value << std::endl << error << std::endl;
        } else {
            os << histo->Integral(min, max) << std::endl;
        }
    } catch (std::exception &e) {
        throw RTKException("RTK Integral failed: ") << e.what();
    }
}

void sumweights(const ArgMap& args, std::ostream& os) {
    try {
        Path path(pipeArg(args.at("<path>").asString()));
        std::unique_ptr<TH1> histo = path.getHistogram();
        double sw = histo->GetSumOfWeights();

        if (args.at("--inclusive").asBool()) {
            if (histo->InheritsFrom("TH2"))
                throw RTKException("Inclusive SumWeights for 2D histograms is not supported yet.");
            sw += histo->GetBinContent(0);
            sw += histo->GetBinContent(histo->GetNbinsX() + 1);
        }

        for (auto &o : args.at("<more>").asStringList()) {
            Path morepath(o);
            std::unique_ptr<TH1> other = morepath.getHistogram();
            sw += other->GetSumOfWeights();

            if (args.at("--inclusive").asBool()) {
                if (histo->InheritsFrom("TH2"))
                    throw RTKException("Inclusive SumWeights for 2D histograms is not supported yet.");
                sw += histo->GetBinContent(0);
                sw += histo->GetBinContent(histo->GetNbinsX() + 1);
            }
        }

        os << sw << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK SumWeights failed: ") << e.what();
    }
}

void projection(const ArgMap& args, std::ostream& os) {
    try {
        Path path(pipeArg(args.at("<path>").asString()));
        std::unique_ptr<TH2> histo((TH2*)path.getHistogram().release());

        TH1D* p;

        const std::string axis = args.at("--axis").asString();
        if (axis == "x") {
            p = histo->ProjectionX();
        } else if (axis == "y") {
            p = histo->ProjectionY();
        } else {
            throw RTKException("Projections may be created only over axis x or y, ")
                << "\"" << axis << "\" given.";
        }

        os << cache(hash(args, 8).c_str(), p) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Projection failed: ") << e.what();
    }
}

void set(const ArgMap& args, std::ostream& os) {
    try {
        std::unique_ptr<TH1> histo = Path(pipeArg(getArg("<path>", args).asString())).getHistogram();

        if (getArg("--title", args))
            histo->SetTitle(args.at("--title").asString().c_str());

        if (getArg("--color", args)) {
            Color_t color = getColor(args.at("--color").asString());
            histo->SetLineColor(color);
            histo->SetFillColor(color);
            histo->SetMarkerColor(color);
        }

        os << cache(hash(args, 8).c_str(), histo.release()) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Set failed: ") << e.what();
    }
}

void binning(const ArgMap& args, std::ostream& os) {
    try {
        std::unique_ptr<TH1> histo = Path(pipeArg(getStringArg("<path>", args))).getHistogram();

        TAxis* axis;
        if (getArg("--axis", args)) {
            if (args.at("--axis").asString() == "x")
                axis = histo->GetXaxis();
            else if (args.at("--axis").asString() == "y")
                axis = histo->GetYaxis();
            else if (args.at("--axis").asString() == "z")
                axis = histo->GetZaxis();
            else
                throw RTKException("Axis ") << args.at("--axis").asString()
                    << " is currently not supported";
        } else {
            axis = histo->GetXaxis();
        }

        Int_t bin;
        Int_t nbins = axis->GetNbins();
        for (bin = 1; bin <= nbins; bin++)
            os << axis->GetBinLowEdge(bin) << ",";
        os << axis->GetBinUpEdge(bin) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Binning failed: ") << e.what();
    }
}

void rebin(const ArgMap& args, std::ostream& os) {
    try {
        auto prefix = getStringArg("--prefix", args);
        auto suffix = getStringArg("--suffix", args);
        std::unique_ptr<TH1> histo = Path(pipeArg(getArg("<path>", args)), prefix, suffix).getHistogram();

        for (auto& path : getArg("<more>", args).asStringList()) {
            std::unique_ptr<TH1> other = Path(path, prefix, suffix).getHistogram();
            histo->Add(other.get());
        }

        if (getArg("--merge", args)) {
            histo = std::unique_ptr<TH1>(histo->Rebin(args.at("--merge").asLong(), "htemp"));
        } else {
            std::vector<double> bins;
            {
                std::vector<std::string> edges;
                boost::split(edges, getArg("<edges>", args).asString(), boost::is_any_of(","));
                for (auto& edge : edges)
                    bins.push_back(std::stod(edge));
            }

            histo = std::unique_ptr<TH1>(histo->Rebin(bins.size() - 1, "htemp", bins.data()));
        }

        if (getArg("--title", args))
            histo->SetTitle(getStringArg("--title", args).c_str());

        if (getArg("--color", args)) {
            Color_t color = args.at("--color").isLong()
                ? args.at("--color").asLong()
                : getColor(args.at("--color").asString());
            histo->SetLineColor(color);
            histo->SetFillColor(color);
            histo->SetMarkerColor(color);
        }

        os << cache(hash(args, 8).c_str(), histo.release()) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Rebin failed: ") << e.what();
    }
}       

void max(const ArgMap& args, std::ostream& os) {
    try {
        auto prefix = getStringArg("--prefix", args);
        auto suffix = getStringArg("--suffix", args);
        std::unique_ptr<TH1> histo = Path(pipeArg(getArg("<path>", args)), prefix, suffix).getHistogram();

        double max = histo->GetBinContent(histo->GetMaximumBin());

        for (auto& path : getArg("<more>", args).asStringList()) {
            std::unique_ptr<TH1> other = Path(path, prefix, suffix).getHistogram();
            max = std::max(max, other->GetBinContent(other->GetMaximumBin()));
        }

        os << max << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Max failed: ") << e.what();
    }
}

void min(const ArgMap& args, std::ostream& os) {
    try {
        auto prefix = getStringArg("--prefix", args);
        auto suffix = getStringArg("--suffix", args);
        std::unique_ptr<TH1> histo = Path(pipeArg(getArg("<path>", args)), prefix, suffix).getHistogram();

        double min = histo->GetBinContent(histo->GetMinimumBin());

        for (auto& path : getArg("<more>", args).asStringList()) {
            std::unique_ptr<TH1> other = Path(path, prefix, suffix).getHistogram();
            min = std::min(min, other->GetBinContent(other->GetMinimumBin()));
        }

        os << min << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Max failed: ") << e.what();
    }
}

void avg(const ArgMap& args, std::ostream& os) {
    try {
        auto prefix = getStringArg("--prefix", args);
        auto suffix = getStringArg("--suffix", args);
        std::unique_ptr<TH1> histo = Path(pipeArg(getArg("<path>", args)), prefix, suffix).getHistogram();

        std::cerr << "Warning: avg over multiple histograms does not account for variable sized bins yet." << std::endl;

        Double_t sum = histo->Integral();
        Int_t count = histo->GetNbinsX() * histo->GetNbinsY();

        for (auto& path : getArg("<more>", args).asStringList()) {
            std::unique_ptr<TH1> other = Path(path, prefix, suffix).getHistogram();
            sum += other->Integral();
            count += histo->GetNbinsX() * histo->GetNbinsY();
        }

        os << sum / count << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Avg failed: ") << e.what();
    }
}

}
