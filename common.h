#ifndef RTK_COMMON_H
#define RTK_COMMON_H

#include <string>
#include <vector>
#include <sys/stat.h>
#include "TH1.h"
#include "docopt.h"

namespace rtk {

using ArgMap = std::map<std::string, docopt::value>;

std::string hash(const std::string &input, const size_t len = 0);
std::string hash(const char* input, const size_t len = 0);
std::string hash(const std::map<std::string, docopt::value> args, const size_t len = 0);

std::string cache(const char* hash, TH1* histo);
std::string write(TH1* histo, const std::string& target, bool recreate = false);
std::string pipeArg(const docopt::value &value);

Int_t getPalette(const std::string& name);
Color_t getColor(const std::string& name);

const docopt::value& getArg(const char* name, const std::map<std::string, docopt::value>& args);
const std::string& getStringArg(const char* name, const std::map<std::string, docopt::value>& args);
const double getDoubleArg(const char* name, const ArgMap& args);

void SET_DEBUG(const bool enabled);
bool DEBUG_ENABLED();

inline bool file_exists (const char* name) {
    struct stat buffer;
    return (stat(name, &buffer) == 0);
}

}

#endif // RTK_COMMON_H
