#ifndef RTK_DRAW_H
#define RTK_DRAW_H

#include <iostream>
#include <sstream>
#include "docopt.h"

namespace rtk {

void draw(const std::map<std::string, docopt::value> args, std::ostream& os = std::cout);

}

#endif // RTK_DRAW_H
