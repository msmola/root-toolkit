#include <stdexcept>
#include <string>
#include <memory>
#include <boost/algorithm/string/split.hpp>
#include "Filelist.h"
#include "common.h"
#include "Path.h"
#include "save.h"
#include "RTKException.h"

namespace rtk {
void save(const std::map<std::string, docopt::value> args, std::ostream& os) {
    try {
        std::unique_ptr<TH1> histo = Path(
                pipeArg(getArg("<source>", args).asString())
                ).getHistogram();

        os << write(
                histo.release(),
                getArg("<target>", args).asString(),
                getArg("--recreate", args).asBool()
                )
            << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Save failed: ") << e.what();
    }
}

}
